import matplotlib
import matplotlib.pyplot as plt
import numpy as np


def plot1(data):
    x_ticks = data.keys()
    tpr = [d['Recall'] for d in data.values()]
    fpr = [d['Confusion'][1] / (d['Confusion'][1] + d['Confusion'][0]) for d in data.values()]
    label_xs = list((np.arange(0, len(data)) - 0.12)) + list((np.arange(0, len(data)) + 0.12))
    label_ys = tpr + fpr
    plt.figure(figsize=(10, 7))
    plt.xticks(np.arange(0, len(data)), tuple(x_ticks))
    plt.yticks(np.arange(0, 1.2, 0.2))
    b1 = plt.bar(x=np.arange(0, len(data)) - 0.12, height=tpr, width=0.2, color='#1946BA', linewidth=1, edgecolor='black')
    b2 = plt.bar(x=np.arange(0, len(data)) + 0.12, height=fpr, width=0.2, color='#EC0B43', linewidth=1, edgecolor='black')
    plt.legend((b1[0], b2[0]), ('True Positive Rate', 'False Positive Rate'), loc=9, bbox_to_anchor=(0.5,-0.08), ncol=2)
    plt.ylabel('Значение метрики')
    for x, y in zip(label_xs, label_ys):
        plt.text(x=x-0.055, y=y+0.02, s=str(np.round(y, 2)))

    ax = plt.gca()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_alpha(0.2)
    ax.grid(alpha=0.5)
    ax.xaxis.grid(False)
    ax.yaxis.grid(True)
    ax.set_axisbelow(True)
    plt.savefig('plots/plot1.png')
    plt.close()


def plot2(data):
    columns_labels = [key for key in data.keys()]
    rows_labels = ['Точность', 'Прецезионность', 'Полнота', 'f-1']
    rows = ['Accuracy', 'Precision', 'Recall', 'F-score']
    cmap = ['#1946BA', '#EC0B43', '#B2B2B2', '#94D600']
    index = np.arange(len(data))
    table_data = []
    plt.figure(figsize=(10, 7))
    for i, row_name in enumerate(rows):
        row = [val[row_name] for val in data.values()]
        plt.bar(x=index + i * 0.180, height=row, width=0.16, color=cmap[i])
        table_data.append([str(np.round(el, 2)) for el in row])
    table_data = np.array(table_data, dtype=np.float)
    table = plt.table(cellText=table_data,
                      rowLabels=rows_labels,
                      colLabels=columns_labels,
                      loc='bottom',
                      bbox=[0, -0.4, 1, 0.4],
                      alpha=0.01)
    table.auto_set_font_size(False)
    table.set_fontsize(10)
    table.scale(1.5, 1.5)
    cell_dict = table.get_celld()
    for i in range(len(columns_labels)):
        cell_dict[(0, i)].set_height(.3)
        for j in range(1, 5):
            pass
            cell_dict[(j, i)].set_height(.16)
    plt.ylabel("Значение метрики")
    plt.xticks([])
    ax = plt.gca()
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_alpha(0.2)
    ax.grid(alpha=0.5)
    ax.xaxis.grid(False)
    ax.yaxis.grid(True)
    ax.set_axisbelow(True)
    plt.savefig('plots/plot2.png', bbox_inches='tight')
    plt.close()

