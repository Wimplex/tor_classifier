import config
import numpy as np
import pandas as pd
import visualizer as vs
from collections import defaultdict
from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from nn_model import NNFlowClassifier


# Предподготовка данных (включает удаление ненужных столбцов и строк и добавление новых столбцов)
def preprocess_data(df, class_label):
    # Удаляем потоки с указанными IP
    df = df[(df['Dst IP'] != '192.168.10.14') | (df['Dst IP'] != '192.168.10.19')]
    # Удаляем ненужные столбцы
    df = df.drop(['Flow ID', 'Src IP', 'Src Port', 'Dst IP', 'Dst Port', 'Timestamp', 'Label', 'Protocol'], axis=1)
    # Удаляем потоки со значением Nan в любом из столбцов
    df = df.dropna()
    # Добавляем поле label
    df['Label'] = class_label
    return df

# Создание смешанного DF, в котором смешанны данные из двух DF с указанными величинами
def get_merged_df(df1, n1, df2, n2):
    df1 = df1.sample(n1)
    df2 = df2.sample(n2)
    mixed_df = pd.DataFrame(pd.concat((df1, df2), axis=0))
    mixed_df = mixed_df.sample(len(mixed_df))
    return mixed_df


def get_confusion_matrix(y_actual, y_true):
    y_actual = np.array(y_actual)
    y_true = np.array(y_true)
    TP = 0  # Мера положительных записей, классифицированных верно
    FP = 0  # Мера отрицательных записей неправильно классифицированных, как положительные
    TN = 0  # Мера негативных записей, классифицированных верно
    FN = 0  # Мера позитивных записей неправильно классифицированных, как отрицательные
    for i in range(len(y_true)):
        if y_actual[i] == y_true[i] == 1:
           TP += 1
        if y_true[i] == 1 and y_actual[i] != y_true[i]:
           FP += 1
        if y_actual[i] == y_true[i] == 0:
           TN += 1
        if y_true[i] == 0 and y_actual[i] != y_true[i]:
           FN += 1
    return [TN, FP, FN, TP]


def eval_model(model, X_test, y_test, model_name, verbose=1):
    res = model.predict(X_test)
    metrics = {}
    TN, FP, FN, TP = metrics['Confusion'] = get_confusion_matrix(res, y_test)
    metrics['Accuracy'] = (TP + TN) / np.sum(metrics['Confusion'])
    pr = metrics['Precision'] = TP / (TP + FP)
    rec = metrics['Recall'] = TP / (TP + FN)
    metrics['F-score'] = 2 * (pr * rec) / (pr + rec)
    if verbose:
        from pprint import pprint
        print('Model {} training results: '.format(model_name))
        pprint(metrics)
    return metrics


# Подготавливаем и мёржим данные
tor_df = pd.read_csv(config.path_to_tor, low_memory=False)
https_df = pd.read_csv(config.path_to_other, low_memory=False)
tor_df = preprocess_data(tor_df, 1)
https_df = preprocess_data(https_df, 0)
mixed_df = get_merged_df(tor_df, len(tor_df), https_df, len(https_df))

# Удаляем столбцы с нулевой дисперсией и сохраняем смёрженый файл
sel = VarianceThreshold()
sel.fit(mixed_df.values)
mixed_df = mixed_df.iloc[:, sel.get_support(indices=True)]
mixed_df.to_csv('data/merged_flows.csv', sep=',')
keys = mixed_df.keys()

# Извлекаем тестовые и тренеровочные X и y
X_train, X_test, y_train, y_test = train_test_split(mixed_df.iloc[:, :-1], mixed_df.iloc[:, -1], train_size=0.9)


importances = defaultdict(list)
results_metrics = {}
models = {
    'Дерево решений': DecisionTreeClassifier(random_state=100, criterion='entropy', max_depth=10),
    'Случайный лес': RandomForestClassifier(n_estimators=27, criterion='entropy',
                                                     max_depth=9, random_state=100),
    'Наивный байесовский\nклассификатор': GaussianNB(var_smoothing=1e-7),
    'Логистическая\nрегрессия': LogisticRegression(solver='liblinear', max_iter=700),
    #'Метод опорных\nвекторов': SVC(gamma='auto'),
    'Нейронная сеть': NNFlowClassifier(X_train.shape)
}

# Обучаем наши модели
for key, model in models.items():
    model.fit(X_train, y_train)
    models[key] = model
    results_metrics[key] = eval_model(models[key], X_test, y_test, key)
    if key == 'Дерево решений':
        importances['Дерево решений'] = model.feature_importances_

# Сохраняем признаки и соответствующие значения важности
with open('imp.txt', 'w') as f:
    f.writelines([str(keys[np.argsort(importances['Дерево решений'])]), str(importances['Дерево решений'])])

# Выводим графики
vs.plot1(results_metrics)
vs.plot2(results_metrics)