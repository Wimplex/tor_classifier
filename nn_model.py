from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import RMSprop
from keras.regularizers import l2


class NNFlowClassifier:
    def __init__(self, input_shape):
        self.build(input_shape)

    def build(self, input_shape):
        _, n_features = input_shape
        self.nn = Sequential([
            Dense(n_features, activation='relu', input_shape=(n_features,), kernel_initializer='normal',
                  kernel_regularizer=l2(1e-4)),
            Dense(30, activation='relu', kernel_initializer='normal', kernel_regularizer=l2(1e-4)),
            Dense(1, activation='sigmoid')
        ])
        self.nn.compile(optimizer=RMSprop(lr=0.01),
                        loss='binary_crossentropy',
                        metrics=['accuracy'])

    def fit(self, X_train, y_train):
        self.nn.fit(x=X_train,
                    y=y_train,
                    batch_size=128,
                    epochs=30,
                    verbose=1)

    def predict(self, X_test):
        return self.nn.predict(X_test)
